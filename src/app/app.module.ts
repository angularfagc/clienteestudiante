import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { FormEstudianteComponent } from './estudiantes/form-estudiante.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes=[
  {path:'',  pathMatch: 'full', redirectTo: '/estudiantes'},
  {path:'estudiantes', component:EstudiantesComponent },
  {path:'estudiantes/form', component:FormEstudianteComponent },
  {path:'estudiantes/form/:id', component:FormEstudianteComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    EstudiantesComponent,
    FormEstudianteComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
