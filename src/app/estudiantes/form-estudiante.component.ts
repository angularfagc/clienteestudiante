import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLinkActive } from '@angular/router';
import { Estudiante } from './estudiante';
import { EstudianteService } from './estudiante.service';

@Component({
  selector: 'app-form-estudiante',
  templateUrl: './form-estudiante.component.html',
  styleUrls: ['./form-estudiante.component.css']
})
export class FormEstudianteComponent implements OnInit {

  estudiante: Estudiante = new Estudiante();
  titulo: string = "Registro de estudiantes";

  constructor(private estudianteService: EstudianteService, private router: Router, private activateRoute: ActivatedRoute) { }

  ngOnInit(): void {
    console.log("Ejecutando metodo...ngOnInit from-estudiente", this.estudiante)
    this.cargar();
  }

  cargar(): void {
    console.log("Eejcutando metodo ...cargar", this.estudiante);
    this.activateRoute.params.subscribe(
      e => {
        let id = e['id'];
        if (id) {
          console.log("valor id=>",id);
          this.estudianteService.get(id).subscribe(
            es => this.estudiante = es            
          );
        } 
      }
    );
  }

  create(): void {
    console.log("Ejecutando metodo ...create", this.estudiante);
    this.estudianteService.create(this.estudiante).subscribe(
      res => this.router.navigate(['/estudiantes'])
    );
  }

  update(): void {
    console.log(this.estudiante);
    this.estudianteService.update(this.estudiante).subscribe(
      res => this.router.navigate(['/estudiantes'])
    );
  }

}
